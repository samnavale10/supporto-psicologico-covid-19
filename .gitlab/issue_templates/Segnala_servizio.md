# Segnalazione nuovo servizio

## Nome del servizio

Scrivi il nome del servizio

## Descrizione

Scrivi un piccolo testo che descrive il servizio

## Telefono

1. numero di telefono
1. numero di telefono 2
1. numero di telefono 3

## SMS

Numero a cui mandare SMS

## WhatsApp

Numero WhatsApp

## Telegram

Contatto Telegram

## Skype

Contatto Skype

## Email

Contatto email

## Sito web

Sito web o link a social

## Orari

Orari in cui è possibile usufruire del servizio

## Luogo

Luogo di riferimento del servizio (città, regione, ...)
