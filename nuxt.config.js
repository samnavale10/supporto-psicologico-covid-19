import pkg from './package'

const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: 'it',
    },
    title: 'Supporto psicologico Covid-19',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
      { hid: 'og:title', property: 'og:title', content: 'Supporto psicologico Covid-19' },
      { hid: 'og:url', property: 'og:url', content: 'https://psicovid19.bedita.net/' },
      { hid: 'og:image', property: 'og:image', content: 'https://psicovid19.bedita.net/share-facebook.png' },
      { hid: 'og:description', property: 'og:description', content: pkg.description },
      { hid: 'twitter:card', property: 'twitter:card', content: 'summary_large_image' },
      { hid: 'twitter:site', property: 'twitter:site', content: '@xho' },
      { hid: 'twitter:creator', property: 'twitter:creator', content: '@xho' },
      { hid: 'twitter:title', property: 'twitter:title', content: 'Supporto psicologico Covid-19' },
      { hid: 'twitter:description', property: 'twitter:description', content: pkg.description },
      { hid: 'twitter:image', property: 'twitter:image', content: 'https://psicovid19.bedita.net/share-twitter.png' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: 'favicon.ico' },
      { rel: 'apple-touch-icon', sizes: '180x180', href: 'apple-touch-icon.png' },
      { rel: 'icon', type: 'image/png', sizes: '32x32', href: 'favicon-32x32.png' },
      { rel: 'icon', type: 'image/png', sizes: '16x16', href: 'favicon-16x16.png' },
      // { rel: 'manifest', href: '/site.webmanifest' },
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#3298dc' },

  /*
  ** Customize the generated output folder
  */
  generate: {
    dir: 'public'
  },

  /*
  ** Customize the base url
  */
  router: {
    base
  },

  /*
  ** Global CSS
  */
  css: [
    '~assets/fonts.css',
    '~assets/line-awesome.min.css',
    '~assets/bulma.min.css'
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
  ],

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
  //   extend(config, ctx) {
  //     // Run ESLint on save
  //     if (ctx.isDev && ctx.isClient) {
  //       config.module.rules.push({
  //         enforce: 'pre',
  //         test: /\.(js|vue)$/,
  //         loader: 'eslint-loader',
  //         exclude: /(node_modules)/
  //       })
  //     }
  //   }
  }
}
