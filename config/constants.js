export const BASEURL = process.env.NODE_ENV === 'production' ? 'https://psicovid19.bedita.net' : "http://localhost:3000";

export const ENDPOINTURL = 'https://r58e03nnfg.execute-api.eu-west-1.amazonaws.com';

export const SEARCHFIELDS = ['title', 'location', 'note'];

export const CATEGORIES = [
    {
        name: 'local',
        label: 'territoriale',
    },
    {
        name: 'migrants',
        label: 'migranti',
    },
    {
        name: 'lgbt',
        label: 'LGBT',
    },
    {
        name: 'multilanguage',
        label: 'multilingua',
    },
    {
        name: 'specific',
        label: 'categorie specifiche',
    },
    {
        name: 'alldays',
        label: 'tutti i giorni',
    },
    {
        name: 'telephone',
        label: 'telefono',
    },
    {
        name: 'web',
        label: 'sito web',
    },
    {
        name: 'whatsapp',
        label: 'whatsapp',
    },
    {
        name: 'skype',
        label: 'skype',
    },
];
